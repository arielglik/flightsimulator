//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_BINARYEXPRESSION_H
#define FLIGHTSIMULATOR_BINARYEXPRESSION_H

#include "Expression.h"

class BinaryExpression : public Expression {
protected:
    Expression *left;
    Expression *right;

public:
    BinaryExpression(Expression *left, Expression *right) : left(left), right(right) {}

    virtual ~BinaryExpression() {
        delete(left);
        delete(right);
    }
};

#endif //FLIGHTSIMULATOR_BINARYEXPRESSION_H
