//
// Created by ariel on 12/9/18.
//


#ifndef FLIGHTSIMULATOR_COMMAND_H
#define FLIGHTSIMULATOR_COMMAND_H

#include <stdio.h>
#include <string>
#include <vector>


using namespace std;

class Command {
public:
    virtual int execute(const vector<string> &words, int index) = 0;

    virtual ~Command() {}
};

#endif //FLIGHTSIMULATOR_COMMAND_H
