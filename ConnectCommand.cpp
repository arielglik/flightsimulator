//
// Created by ariel on 12/20/18.
//

#include <cstring>
#include "ConnectCommand.h"
#include "ShuntingYard.h"
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>


void *writeToSimulator(void *arg) {
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    struct ClientParams *params = (struct ClientParams *) arg;

    char buffer[256];
    portno = params->port;

    /* Create a socket point */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    params->sockfd = sockfd;

    if (sockfd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }

    server = gethostbyname(params->ip.c_str());

    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    /* Now connect to the server */
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR connecting");
        exit(1);
    }

    /* Now ask for a message from the user, this message
       * will be read by server
    */

    while (!params->isNeedToStop) {
        if (params->isNeedToSet) {
            bzero(buffer, 256);
            for (int i = 0; i < params->text.length(); i++) {
                buffer[i] = params->text[i];
            }

            /* Send message to the server */
            n = write(sockfd, buffer, strlen(buffer));

            if (n < 0) {
                perror("ERROR writing to socket");
                exit(1);
            }

            /* Now read server response */
            bzero(buffer, 256);
            n = read(sockfd, buffer, 255);

            if (n < 0) {
                perror("ERROR reading from socket");
                exit(1);
            }
            string str(buffer);
            params->text = str;
            params->isNeedToSet = false;
        }
    }
}

int ConnectCommand::execute(const vector<string> &words, int index) {
    string ip = words.at(index + 1);
    unordered_map<string, Var *> varMap;
    ShuntingYard *shuntingYard = new ShuntingYard(varMap);
    vector<string> portVec;
    portVec.push_back(words.at(index + 2));
    portVec.push_back(";");
    int port = (int) shuntingYard->calculateExpression(portVec, 0);
    clientParams->ip = ip;
    clientParams->port = port;

    pthread_t trid;
    pthread_create(&trid, nullptr, writeToSimulator, (void *) clientParams);

    delete (shuntingYard);
    return 4;
}

ConnectCommand::ConnectCommand() {
    clientParams = new ClientParams();
}

ConnectCommand::~ConnectCommand() {
    delete (clientParams);
}

ClientParams::ClientParams() {
    text = "";
    isNeedToSet = false;
    isNeedToStop = false;
}

