//
// Created by ariel on 12/20/18.
//

#ifndef FLIGHTSIMULATOR_CONNECTCOMMAND_H
#define FLIGHTSIMULATOR_CONNECTCOMMAND_H

#include "Command.h"
#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <strings.h>

using namespace std;

struct ClientParams {
    string ip;
    int port;
    int sockfd;
    string text;
    bool isNeedToSet;
    bool isNeedToStop;

    ClientParams();
};

class ConnectCommand : public Command {

public:
    struct ClientParams *clientParams;

    int execute(const vector<string> &words, int index) override;

    ConnectCommand();

    virtual ~ConnectCommand();
};


#endif //FLIGHTSIMULATOR_CONNECTCOMMAND_H