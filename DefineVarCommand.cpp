//
// Created by nir on 12/16/18.
//

#include "DefineVarCommand.h"

int DefineVarCommand::execute(const vector<string> &words, int index) {
    Var *var = new Var(words.at(index + 1), 0, clientParams);
    varsMap[words.at(index + 1)] = var;

    return 2; // ex: var rudder = ...
}

DefineVarCommand::DefineVarCommand(unordered_map<string, Var *> &varsMap, ClientParams *clientParams) : varsMap(
        varsMap), clientParams(clientParams) {}