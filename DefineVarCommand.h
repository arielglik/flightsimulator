//
// Created by nir on 12/16/18.
//

#ifndef FLIGHTSIMULATOR_DEFINEVARCOMMAND_H
#define FLIGHTSIMULATOR_DEFINEVARCOMMAND_H


#include "Command.h"
#include "EqualCommand.h"

class DefineVarCommand : public Command {

private:
    unordered_map<string, Var *> &varsMap;


public:
    struct ClientParams *clientParams;

    int execute(const vector<string> &words, int index) override;

    DefineVarCommand(unordered_map<string, Var *> &varsMap, ClientParams *clientParams);
};


#endif //FLIGHTSIMULATOR_DEFINEVARCOMMAND_H
