//
// Created by ariel on 12/23/18.
//

#ifndef FLIGHTSIMULATOR_ENTERCHARCOMMAND_H
#define FLIGHTSIMULATOR_ENTERCHARCOMMAND_H

#include <iostream>
#include "Command.h"


using namespace std;

class EnterCharCommand : public Command {
public:
    EnterCharCommand();

    int execute(const vector<string> &words, int index) override;

    virtual ~EnterCharCommand();
};


#endif //FLIGHTSIMULATOR_ENTERCHARCOMMAND_H
