//
// Created by ariel on 12/14/18.
//

#include "EqualCommand.h"

int EqualCommand::execute(const vector<string> &words, int index) {
    string str = words.at(index - 1); // assuming index is on the '='
    int counter = 0;
    unordered_map<string, Var *>::const_iterator it = varsMap.find(words.at(index - 1));
    Var *var = it->second;
    double *bindedValue;
    double value = 0;
    string bindedString, curPath;

    if (words.at(index + 1) == "bind") {
        list<PathAndValue *>::iterator it;

        for (it = pathAndValueList.begin(); it != pathAndValueList.end(); it++) {
            bindedString = words.at(index + 2);
            curPath = (*it)->path;
            if ((*it)->path == bindedString) {
                bindedValue = &(*it)->value;
                var->setBinding(bindedString, bindedValue, true);
                break;
            }
        }
        unordered_map<string, Var *>::iterator itVar = varsMap.find(words.at(index + 2));

        if (itVar != varsMap.end() && it == pathAndValueList.end()) {
            *bindedValue = itVar->second->getValue();
            bindedString = itVar->second->getBindAddress();
            var->setBinding(bindedString, bindedValue, true);
        }

        if (it == pathAndValueList.end() && itVar == varsMap.end()) { // Not in XML file
            bindedValue = &value;
            var->setBinding(bindedString, bindedValue, false);
        }
    } else {
        value = shuntingYard->calculateExpression(words, index + 1);
        //*bindedValue = value;
        if (var->getIsBinded()) {
            var->setBindedValue(&value);
        } else {
            var->setValue(value);
        }
    }

    str = words.at(index);
    for (index; str != ";"; index++, counter++) {
        str = words.at(index);
    }

    return counter;
}

EqualCommand::EqualCommand(unordered_map<string, Var *> &varsMap, list<PathAndValue *> &pathAndValueList)
        : varsMap(
        varsMap), pathAndValueList(pathAndValueList) {
    this->shuntingYard = new ShuntingYard(varsMap);
}

EqualCommand::~EqualCommand() {
    delete this->shuntingYard;
}

