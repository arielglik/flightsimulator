//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_EQUALCOMMAND_H
#define FLIGHTSIMULATOR_EQUALCOMMAND_H


#include "Command.h"
#include "Var.h"
#include "ShuntingYard.h"
#include <tr1/unordered_map>
#include "OpenServerCommand.h"

using namespace std;
using namespace tr1;

class EqualCommand : public Command {

private:
    unordered_map<string, Var *> &varsMap;
    ShuntingYard *shuntingYard;
    list<PathAndValue *> &pathAndValueList;

public:
    EqualCommand(unordered_map<string, Var *> &varsMap, list<PathAndValue *> &pathAndValueList);

    int execute(const vector<string> &words, int index) override;

    ~EqualCommand();
};


#endif //FLIGHTSIMULATOR_EQUALCOMMAND_H
