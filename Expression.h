//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_EXPRESSION_H
#define FLIGHTSIMULATOR_EXPRESSION_H


using namespace std;

class Expression {
public:
    virtual double calculate() = 0;

    virtual ~Expression() {
    }
};

#endif //FLIGHTSIMULATOR_EXPRESSION_H
