//
// Created by nir on 12/16/18.
//

#include "IfCommand.h"
#include "LoopCommand.h"

int IfCommand::insertAllCommnads(int index) { // index point on - while/if
    int firstWordIndex = index;
    string curWord;
    int i;
    int counter = 1;

    for (; words.at(index) != ";"; index++) {}

    for (++index; words.at(index) != "}"; index++) {

        curWord = words.at(index);
        unordered_map<string, Command *>::iterator it = commandsMap.find(curWord);

        if (words.at(index) == "if" || words.at(index) == "while") {
            if (words.at(index) == "if") {
                commandsLst.push_back(new IfCommand(commandsMap, varsMap, words));
            } else {
                commandsLst.push_back(new LoopCommand(commandsMap, varsMap, words));
            }
            commandsLstIndexes.push_back(index);
            for (i = index; words.at(i)[0] != '{'; ++i) {}
            i++;
            while (counter != 0) {
                if (words.at(i)[0] == '{') {
                    counter++;
                } else if (words.at(i)[0] == '}') {
                    counter--;
                }
                i++;
            }
            index += i - index;
        } else if (it != commandsMap.end()) {
            commandsLst.push_back(it->second);
            commandsLstIndexes.push_back(index);

        }
    }
    return index - firstWordIndex + 2; // +2 for "while" and ';'
}

bool IfCommand::checkCondition(int index) { // index point on - while/if
    vector<string> leftExp;
    vector<string> rightExp;
    int indexOfOperator = findOperatorIndex(index);
    leftExp = createLeftExp(index + 1, indexOfOperator);
    rightExp = createrightExp(indexOfOperator + 1);

    double leftValue = shuntingYard->calculateExpression(leftExp, 0);
    double rightValue = shuntingYard->calculateExpression(rightExp, 0);
    string op = words.at(indexOfOperator);

    switch (op.length()) {
        case 1: {
            switch (op[0]) {
                case '<': {
                    if (leftValue < rightValue) {
                        return true;
                    } else {
                        return false;
                    }
                }
                case '>': {
                    if (leftValue > rightValue) {
                        return true;
                    } else {
                        return false;
                    }
                }
                default:
                    throw "Invalid operator.";
            }
        }
        case 2: {
            if (op == "<=") {
                if (leftValue <= rightValue) {
                    return true;
                } else {
                    return false;
                }
            } else if (op == ">=") {
                if (leftValue >= rightValue) {
                    return true;
                } else {
                    return false;
                }
            } else if (op == "==") {
                if (leftValue == rightValue) {
                    return true;
                } else {
                    return false;
                }
            } else if (op != "!=") {
                if (leftValue != rightValue) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        default:
            throw "Invalid operator";
    }
}

int IfCommand::findOperatorIndex(int index) {
    char ch;
    for (; words.at(index) != "{"; index++) {
        ch = words.at(index)[0];
        if (ch == '<' || ch == '>' || ch == '!' || ch == '=') {
            return index;
        }
    }
    return -1;
}

vector<string> IfCommand::createLeftExp(int start, int end) {
    vector<string> leftExp;

    for (; start < end; start++) {
        leftExp.push_back(words.at(start));
    }
    leftExp.push_back(";");
    return leftExp;
}

vector<string> IfCommand::createrightExp(int start) {
    vector<string> rightExp;
    for (; words.at(start) != "{"; start++) {
        rightExp.push_back(words.at(start));
    }
    rightExp.push_back(";");
    return rightExp;
}

IfCommand::IfCommand(unordered_map<string, Command *> &commandsMap,
                     const unordered_map<string, Var *> &varsMap, const vector<string> &words) : commandsMap(
        commandsMap), varsMap(varsMap), words(words) {
    this->shuntingYard = new ShuntingYard(varsMap);
    needToCheckCondition = true;
    isCommandsListLoaded = false;
}

int IfCommand::execute(const vector<string> &words, int index) {
    if (!isCommandsListLoaded) {
        returnedValue = insertAllCommnads(index);
        isCommandsListLoaded = true;
    }

    bool condition;
    if (needToCheckCondition) {
        condition = checkCondition(index);
    } else {
        condition = knownCondition;
    }


    if (condition) {
        list<Command *>::iterator it = commandsLst.begin();
        list<int>::iterator itIndexes = commandsLstIndexes.begin();

        for (; it != commandsLst.end(); it++, itIndexes++) {
            (*it)->execute(words, *itIndexes);

            int y = 5;
        }
    }
    if (needToCheckCondition) {
//        list<Command *>::iterator it = commandsLst.begin();
//        for (; it != commandsLst.end(); it++) {
//            delete (&it);
//        }
        commandsLst.clear();
    }

    return returnedValue;
}

void IfCommand::setKnownCondition(bool knownCondition) {
    IfCommand::knownCondition = knownCondition;
}

IfCommand::IfCommand(unordered_map<string, Command *> &commandsMap, const unordered_map<string, Var *> &varsMap,
                     const vector<string> &words, bool needToCheckCondition) : commandsMap(
        commandsMap), varsMap(varsMap), words(words), needToCheckCondition(needToCheckCondition) {
    this->shuntingYard = new ShuntingYard(varsMap);
    isCommandsListLoaded = false;
}

int IfCommand::getReturnedValue() const {
    return returnedValue;
}

IfCommand::~IfCommand() {
    delete (this->shuntingYard);
}
