//
// Created by nir on 12/16/18.
//

#ifndef FLIGHTSIMULATOR_IFCOMMAND_H
#define FLIGHTSIMULATOR_IFCOMMAND_H

#include <list>
#include <map>
#include <tr1/unordered_map>
#include "Command.h"
#include "ShuntingYard.h"

using namespace std;
using namespace tr1;

class IfCommand : public Command {

protected:
    unordered_map<string, Command *> &commandsMap;
    const unordered_map<string, Var *> &varsMap;
    ShuntingYard *shuntingYard;
    const vector<string> &words;
    bool needToCheckCondition;
    bool knownCondition;
    int returnedValue;


    int insertAllCommnads(int index);


    int findOperatorIndex(int index);

    vector<string> createLeftExp(int start, int end);

    vector<string> createrightExp(int start);


public:
    bool isCommandsListLoaded;
    list<Command *> commandsLst;
    list<int> commandsLstIndexes;

    IfCommand(unordered_map<string, Command *> &commandsMap, const unordered_map<string, Var *> &varsMap,
              const vector<string> &words);

    IfCommand(unordered_map<string, Command *> &commandsMap, const unordered_map<string, Var *> &varsMap,
              const vector<string> &words, bool needToCheckCondition);

    int execute(const vector<string> &words, int index) override;

    bool checkCondition(int index);

    void setKnownCondition(bool knownCondition);

    int getReturnedValue() const;

    virtual ~IfCommand();
};


#endif //FLIGHTSIMULATOR_IFCOMMAND_H
