//
// Created by ariel on 12/9/18.
//

#include <iostream>
#include "Lexer.h"

vector<string> Lexer::lexerFromFile(string path, bool isReadFromFile) {
    string line;

    if (isReadFromFile) {
        ifstream in(path);
        if (in.is_open()) {
            while (getline(in, line)) {
                splitLine(line, words);
            }
            in.close();
        }
    } else {
        getline(cin, line);
        splitLine(line, words);
    }
    return words;
}

void Lexer::splitLine(string line, vector<string> &words) {
    int i, k, index = 0;
    string str;

    for (i = 0, k = 0; k < line.length(); i = ++k) {
        for (; line[i] == ' ' || line[i] == '\t' || line[i] == '\"' || line[i] == ','; i++) {}
        for (k = i; line[k] != ' ' && k < line.length(); k++) {}
        str = line.substr(i, k - i);
        if (str[str.length() - 1] == '\"') {
            str = str.substr(0, str.length() - 1);
        }

        if (str == "print" && line.find('\"') != -1) {
            words.push_back(str);
            words.push_back("printAsStringFlag");
        } else {
            words.push_back(str);
        }
        index++;
    }
    words.push_back(";"); // indicates end of line.
}

Lexer::Lexer(vector<string> &words) : words(words) {}
