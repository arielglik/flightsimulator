//
// Created by ariel on 12/9/18.
//

#ifndef FLIGHTSIMULATOR_LEXER_H
#define FLIGHTSIMULATOR_LEXER_H

#include <string>
#include <fstream>
#include <vector>

using namespace std;

class Lexer {

public:
    vector<string> &words;

    vector<string> lexerFromFile(string path, bool isReadFromFile);

    void splitLine(string line, vector<string> &words);

    Lexer(vector<string> &words);
};


#endif //FLIGHTSIMULATOR_LEXER_H
