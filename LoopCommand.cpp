//
// Created by ariel on 12/14/18.
//

#include "LoopCommand.h"

int LoopCommand::execute(const vector<string> &words, int index) {
    int returnedValue;
    bool condition = ifCommand->checkCondition(index);
    ifCommand->setKnownCondition(condition);

    while (condition) {
        ifCommand->execute(words, index);
        condition = ifCommand->checkCondition(index);
    }
    returnedValue = ifCommand->getReturnedValue();
    ifCommand->commandsLst.clear();
    ifCommand->commandsLstIndexes.clear();
    ifCommand->isCommandsListLoaded = false;

    return returnedValue;
}

LoopCommand::~LoopCommand() {
    delete (this->ifCommand);
}

LoopCommand::LoopCommand(unordered_map<string, Command *> &commandsMap, const unordered_map<string, Var *> &varsMap,
                         const vector<string> &words) {
    this->ifCommand = new IfCommand(commandsMap, varsMap, words, false);
}