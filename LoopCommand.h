//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_WHILECOMMAND_H
#define FLIGHTSIMULATOR_WHILECOMMAND_H

#include <tr1/unordered_map>
#include "Command.h"
#include "Var.h"
#include "IfCommand.h"


using namespace std;
using namespace tr1;

class LoopCommand : public Command {

private:
    IfCommand *ifCommand;

public:
    LoopCommand(unordered_map<string, Command *> &commandsMap, const unordered_map<string, Var *> &varsMap,
                const vector<string> &words);

    int execute(const vector<string> &words, int index) override;

    virtual ~LoopCommand();
};


#endif //FLIGHTSIMULATOR_WHILECOMMAND_H
