//
// Created by ariel on 12/14/18.
//

#include "BinaryExpression.h"

class Minus : public BinaryExpression {

public:

    Minus(Expression *left, Expression *right) : BinaryExpression(left, right) {}

    double calculate() override {
        return left->calculate() - right->calculate();
    }
};
