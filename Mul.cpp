//
// Created by ariel on 12/14/18.
//

#include "BinaryExpression.h"

class Mul : public BinaryExpression {

public:

    Mul(Expression *left, Expression *right) : BinaryExpression(left, right) {}

    double calculate() override {
        return left->calculate() * right->calculate();
    }
};
