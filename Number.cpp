//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_NUMBER_H
#define FLIGHTSIMULATOR_NUMBER_H

#include "Expression.h"


using namespace std;

class Number : public Expression {

private:
    double number;

public:
    Number(double number) : number(number) {}

    virtual ~Number() {
    }

    double calculate() override {
        return this->number;
    }
};

#endif //FLIGHTSIMULATOR_NUMBER_H
