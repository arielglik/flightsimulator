//
// Created by ariel on 12/9/18.
//

#include "OpenServerCommand.h"

void *readFromSimulator(void *arg) {
    struct ServerParams *params = (struct ServerParams *) arg;
    int sockfd, newsockfd, portno, clilen;
    char buffer[256];
    struct sockaddr_in serv_addr, cli_addr;
    int i, j, n;



    /* First call to socket() function */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    params->sockfd = sockfd;

    if (sockfd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }

    /* Initialize socket structure */
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = params->port;

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    /* Now bind the host address using bind() call.*/
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR on binding");
        exit(1);
    }

    /* Now start listening for the clients, here process will
       * go in sleep mode and will wait for the incoming connection
    */
    listen(sockfd, 10);
    clilen = sizeof(cli_addr);

    /* Accept actual connection from the client */
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, (socklen_t *) &clilen);

    if (newsockfd < 0) {
        perror("ERROR on accept");
        exit(1);
    }
    list<PathAndValue *>::iterator it = params->pathAndValueLst.begin();
    bool isReminder = false;
    string unFinishedValue;
    /* If connection is established then start communicating */
    while (!params->shouldStopRead) {
        bzero(buffer, 256);
        n = read(newsockfd, buffer, 256);
        string str(buffer);

        for (i = 0; i < str.length(); i++) {
            for (j = i; str[j] != ',' && str[j] != '\n' && j < str.length(); j++) {}

            if (j >= str.length()) {
                isReminder = true;
                unFinishedValue = str.substr(i, j - i);
                break;
            }
            if (isReminder) {
                isReminder = false;
                unFinishedValue += str.substr(i, j - i);
                (*it)->value = stod(unFinishedValue);
                unFinishedValue = "";
            } else {
                (*it)->value = stod(str.substr(i, j - i));
            }
            it++;

            if (str[j] == '\n') {
                it = params->pathAndValueLst.begin();
            }
            i = j;
        }

        while (!buffer)

            if (n < 0) {
                perror("ERROR reading from socket");
                exit(1);
            }
//        cout<<buffer<<endl;
    }

}


int OpenServerCommand::execute(const vector<string> &words, int index) {
    unordered_map<string, Var *> varMap;
    ShuntingYard *shuntingYard = new ShuntingYard(varMap);
    vector<string> portVec;
    vector<string> beatsVec;
    portVec.push_back(words.at(index + 1));
    portVec.push_back(";");
    beatsVec.push_back(words.at(index + 2));
    beatsVec.push_back(";");
    int port = (int) shuntingYard->calculateExpression(portVec, 0);
    int beats = (int) shuntingYard->calculateExpression(beatsVec, 0);
    delete (shuntingYard);
    serverParams->port = port;
    serverParams->beats = beats;

    pthread_t trid;
    pthread_create(&trid, nullptr, readFromSimulator, (void *) serverParams);


    return 4;
}

OpenServerCommand::OpenServerCommand(list<PathAndValue *> &pathAndValueList) : pathAndValueList(pathAndValueList) {
    serverParams = new ServerParams(pathAndValueList);
}

ServerParams *OpenServerCommand::getServerParams() {
    return serverParams;
}

OpenServerCommand::~OpenServerCommand() {
    delete (serverParams);
}

PathAndValue::PathAndValue(const string &path) : path(path) {}

PathAndValue::~PathAndValue() {

}
