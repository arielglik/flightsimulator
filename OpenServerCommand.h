//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_OPENSERVERCOMMAND_H
#define FLIGHTSIMULATOR_OPENSERVERCOMMAND_H

#include "Command.h"
#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>

#include <string.h>

#include <sys/socket.h>
#include <pthread.h>
#include <iostream>
#include <map>
#include <list>
#include "ShuntingYard.h"


using namespace std;

struct PathAndValue {
    string path;
    double value;

    PathAndValue(const string &path);

    virtual ~PathAndValue();
};


struct ServerParams {
    int port;
    int beats;
    int sockfd;
    list<PathAndValue *> &pathAndValueLst;
    bool shouldStopRead;

    ServerParams(list<PathAndValue *> &pathAndValueLst) : pathAndValueLst(pathAndValueLst) {
        shouldStopRead = false;
    }
};


class OpenServerCommand : public Command {
private:
    struct ServerParams *serverParams;

public:
    list<PathAndValue *> &pathAndValueList;

    ServerParams *getServerParams();

    int execute(const vector<string> &words, int index) override;

    OpenServerCommand(list<PathAndValue *> &pathAndValueList);

    virtual ~OpenServerCommand();
};

#endif //FLIGHTSIMULATOR_OPENSERVERCOMMAND_H