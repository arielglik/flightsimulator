//
// Created by ariel on 12/14/18.
//

#include "Parser.h"

void Parser::initialCommandsMap() {
    OpenServerCommand *openServerCommand = new OpenServerCommand(this->pathAndValueList);
    this->commandsMap["openDataServer"] = openServerCommand;
    this->serverParams = openServerCommand->getServerParams();
    this->commandsMap["="] = new EqualCommand(varsMap, pathAndValueList);
    ConnectCommand *connectCommand = new ConnectCommand();
    this->commandsMap["connect"] = connectCommand;
    clientParams = connectCommand->clientParams;
    this->commandsMap["var"] = new DefineVarCommand(varsMap, clientParams);
    this->commandsMap["if"] = new IfCommand(commandsMap, varsMap, words);
    this->commandsMap["while"] = new LoopCommand(commandsMap, varsMap, words);
    this->commandsMap["print"] = new PrintCommand(varsMap);
    this->commandsMap["sleep"] = new SleepCommand(varsMap);
    this->commandsMap["Enterc"] = new EnterCharCommand();
}


void Parser::initalizeListValues() {
    pathAndValueList.push_back(new PathAndValue("/instrumentation/airspeed-indicator/indicated-speed-kt"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/altimeter/indicated-altitude-ft"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/altimeter/pressure-alt-ft"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/attitude-indicator/indicated-pitch-deg"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/attitude-indicator/indicated-roll-deg"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/attitude-indicator/internal-pitch-deg"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/attitude-indicator/internal-roll-deg"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/encoder/indicated-altitude-ft"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/encoder/pressure-alt-ft"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/gps/indicated-altitude-ft"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/gps/indicated-ground-speed-kt"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/gps/indicated-vertical-speed"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/heading-indicator/indicated-heading-deg"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/magnetic-compass/indicated-heading-deg"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/slip-skid-ball/indicated-slip-skid"));
    pathAndValueList.push_back(new PathAndValue("/instrumentation/turn-indicator/indicated-turn-rate"));
    pathAndValueList.push_back(
            new PathAndValue("/instrumentation/vertical-speed-indicator/indicated-speed-fpm"));
    pathAndValueList.push_back(new PathAndValue("/controls/flight/aileron"));
    pathAndValueList.push_back(new PathAndValue("/controls/flight/elevator"));
    pathAndValueList.push_back(new PathAndValue("/controls/flight/rudder"));
    pathAndValueList.push_back(new PathAndValue("/controls/flight/flaps"));
    pathAndValueList.push_back(new PathAndValue("/controls/engines/current-engine/throttle"));
    pathAndValueList.push_back(new PathAndValue("/engines/engine/rpm"));

    list<PathAndValue *>::iterator it;
    for (it = pathAndValueList.begin(); it != pathAndValueList.end(); it++) {
        double d = 0;
        (*it)->value = d;
    }
}

Parser::Parser(vector<string> &words) : words(words) {
    initalizeListValues();
    initialCommandsMap();
    index = 0;
}


bool Parser::run() {
    unordered_map<string, Command *>::iterator it;
    string str;
    bool isFinished = false;

    for (index = 0; index < words.size();) {
        str = words.at(index);
        it = commandsMap.find(str);

        if (it != commandsMap.end()) {
            index += it->second->execute(words, index);
        } else if (str == "exit") {
            isFinished = true;
            break;
        } else {
            index++;
        }
    }

    if (isFinished || index >= words.size()) {
        closeAll();
        return true;
    } else {
        return false;
    }

}

void Parser::closeAll() {
    close(this->serverParams->sockfd);
    close(this->clientParams->sockfd);
    this->serverParams->shouldStopRead = true;
    this->clientParams->isNeedToStop = true;


    for (unordered_map<string, Var *>::iterator itVar = varsMap.begin(); itVar != varsMap.end(); itVar++) {
        delete (itVar->second);
    }

    int i;
    unordered_map<string, Command *>::iterator itCommand;
    for (i = 0, itCommand = commandsMap.begin();
         itCommand != commandsMap.end() && i < commandsMap.size(); itCommand++, i++) {

        delete (itCommand->second);
    }

    for (list<PathAndValue *>::iterator itPathAndValue = pathAndValueList.begin();
         itPathAndValue != pathAndValueList.end(); itPathAndValue++) {
        string text = (*itPathAndValue)->path;
        delete (*itPathAndValue);
    }
    varsMap.clear();
    commandsMap.clear();
    pathAndValueList.clear();
    words.clear();
}