//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_PARSER_H
#define FLIGHTSIMULATOR_PARSER_H

#include "Var.h"
#include "Expression.h"
#include "ShuntingYard.h"
#include "OpenServerCommand.h"

#include <tr1/unordered_map>
#include "Expression.h"
#include "EqualCommand.h"
#include "DefineVarCommand.h"
#include "ConnectCommand.h"
#include "SleepCommand.h"
#include "PrintCommand.h"
#include "OpenServerCommand.h"
#include "IfCommand.h"
#include "LoopCommand.h"
#include "EnterCharCommand.h"

using namespace std;
using namespace tr1;

class Parser {

private:
    unordered_map<string, Var *> varsMap;
    unordered_map<string, Command *> commandsMap;
    vector<string> &words; // initial by lexer.
    list<PathAndValue *> pathAndValueList;
    struct ServerParams *serverParams;
    struct ClientParams *clientParams;
    int index;


public:

    Parser(vector<string> &words);

    void initalizeListValues();

    void initialCommandsMap();

    bool run();

    void closeAll();
};


#endif //FLIGHTSIMULATOR_PARSER_H
