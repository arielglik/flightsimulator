//
// Created by ariel on 12/14/18.
//

#include "BinaryExpression.h"

class Plus : public BinaryExpression {

public:

    Plus(Expression *left, Expression *right) : BinaryExpression(left, right) {}

    double calculate() override {
        return left->calculate() + right->calculate();
    }
};
