//
// Created by nir on 12/20/18.
//

#include "PrintCommand.h"


int PrintCommand::execute(const vector<string> &words, int index) {
    string str = words.at(index + 1); // assuming index is on the 'print'
    string printValue = "";
    int counter = 0;
    unordered_map<string, Var *>::const_iterator it = varsMap.find(str);

    if (str == "printAsStringFlag") {
        str = words.at(index + 2);
        for (int i = 3; str != ";"; i++) {
            printValue += str;
            str = words.at(index + i);
            if (str != ";") {
                printValue += " ";
            }
        }
        cout << printValue << endl;
    } else {
        double value = shuntingYard->calculateExpression(words, index + 1);
        cout << value << endl;
    }
    str = "";
    for (index; str != ";"; index++, counter++) {
        str = words.at(index);
    }

    return counter;
}

PrintCommand::PrintCommand(unordered_map<string, Var *> &varsMap) : varsMap(varsMap) {
    this->shuntingYard = new ShuntingYard(varsMap);
}

PrintCommand::~PrintCommand() {
    delete (this->shuntingYard);
}


