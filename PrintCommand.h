//
// Created by nir on 12/20/18.
//

#ifndef FLIGHTSIMULATOR_PRINTCOMMAND_H
#define FLIGHTSIMULATOR_PRINTCOMMAND_H


#include "Command.h"
#include "Var.h"
#include "OpenServerCommand.h"
#include "ShuntingYard.h"
#include <tr1/unordered_map>

using namespace std;
using namespace tr1;

class PrintCommand : public Command{

private:
    unordered_map<string, Var *> &varsMap;
    ShuntingYard *shuntingYard;

public:
    PrintCommand(unordered_map<string, Var *> &varsMap);

    int execute(const vector<string> &words, int index) override;

    virtual ~PrintCommand();
};


#endif //FLIGHTSIMULATOR_PRINTCOMMAND_H
