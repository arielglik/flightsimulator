//
// Created by ariel on 12/14/18.
//

#include <iostream>
#include "ShuntingYard.h"
#include <typeinfo>

string ShuntingYard::makeValueFromExpression(const vector<string> &words, int index) {
    int i, k;
    vector<string> mathExp;
    string expStr;
    string finalExp;
    string key;
    char ch;

    for (i = index; words.at(i) != ";"; i++) {
        expStr += words.at(i);
    }


    for (i = 0; i < expStr.length(); i++) {
        ch = expStr[i];
        if (isOperator(ch) || isNumber(ch) || ch == '.') {
            finalExp += expStr[i];
            continue;
        } else {
            for (k = i + 1; k <= expStr.length(); k++) {
                key = expStr.substr(i, k - i);
                unordered_map<string, Var *>::const_iterator it = varsMap.find(key);
                if (!(it == varsMap.end())) { // didnt find var
                    finalExp += to_string(it->second->getValue());
                    i += key.length() - 1;
                    break;
                }
            }
        }
    }
    mathExp.push_back(finalExp);
    mathExp.push_back(";");
    finalExp = fromManyToOneString(mathExp, 0);
    return finalExp;
}

bool ShuntingYard::isOperator(char ch) {
    switch (ch) {
        case '(':
            return true;

        case ')':
            return true;

        case '+':
            return true;

        case '-':
            return true;

        case '*':
            return true;

        case '/':
            return true;

        case '~':
            return true;

        default:
            return false;
            break;
    }
}

bool ShuntingYard::isNumber(char ch) {

    if (ch > 47 && ch < 58) {
        return true;
    } else {
        return false;
    }
}

string ShuntingYard::fromManyToOneString(const vector<string> &words, int index) {
    string ex = "";
    string finalEx = "";
    int i, j, k;

    for (i = index; words.at(i) != ";"; i++) {
        ex += words.at(i);
    }
    i = 0;

    // Edge Case
    if (ex[0] == '-' && ex[1] == '-') {
        i = 2;
    } else if (ex[0] == '-') {

        finalEx += "(0-";
        for (j = 1; (!isOperator(ex[j]) || ex[j] == '.') && j < ex.length(); j++) {
            finalEx += ex[j];
        }
        finalEx += ")";
        i += j;
    }

    for (; i < ex.length(); i++) {
        if (ex[i] == '-' && ex[i + 1] == '-') {
            finalEx += "+";
            i++;
        } else if (ex[i + 1] == '-' && isOperator(ex[i])) {
            finalEx += ex[i];
            finalEx += "(0-";
            for (k = 1, j = i + 2; isNumber(ex[j]) || ex[j] == '.'; j++, k++) {
                finalEx += ex[j];
            }
            finalEx += ")";
            i += k;
        } else {
            finalEx += ex[i];
        }
    }

    return finalEx;
}

deque<Token> ShuntingYard::exprToTokens(const std::string &expr) {
    std::deque<Token> tokens;

    for (const auto *p = expr.c_str(); *p; ++p) {
        if (isdigit(*p)) {
            const auto *b = p;
            for (; isdigit(*p) || p[0] == '.'; ++p) { ;
            }
            const auto s = std::string(b, p);
            tokens.push_back(Token{Token::Type::Number, s});
            --p;
        } else {
            Token::Type t = Token::Type::Unknown;
            int pr = -1;            // precedence
            bool ra = false;        // rightAssociative
            switch (*p) {
                case '(':
                    t = Token::Type::LeftParen;
                    break;
                case ')':
                    t = Token::Type::RightParen;
                    break;
                case '*':
                    t = Token::Type::Operator;
                    pr = 3;
                    break;
                case '/':
                    t = Token::Type::Operator;
                    pr = 3;
                    break;
                case '+':
                    t = Token::Type::Operator;
                    pr = 2;
                    break;
                case '-':
                    t = Token::Type::Operator;
                    pr = 2;
                    break;


                default:
                    break;
            }
            tokens.push_back(Token{
                    t, std::string(1, *p), pr, ra
            });
        }
    }
    return tokens;
}

deque<Token> ShuntingYard::shuntingYard(const std::deque<Token> &tokens) {
    std::deque<Token> queue;
    std::vector<Token> stack;

    // While there are tokens to be read:
    for (auto token : tokens) {
        // Read a token
        switch (token.type) {
            case Token::Type::Number:
                // If the token is a number, then add it to the output queue
                queue.push_back(token);
                break;

            case Token::Type::Operator: {
                // If the token is operator, o1, then:
                const auto o1 = token;

                // while there is an operator token,
                while (!stack.empty()) {
                    // o2, at the top of stack, and
                    const auto o2 = stack.back();

                    // either o1 is left-associative and its precedence is
                    // *less than or equal* to that of o2,
                    // or o1 if right associative, and has precedence
                    // *less than* that of o2,
                    if (
                            (!o1.rightAssociative && o1.precedence <= o2.precedence)
                            || (o1.rightAssociative && o1.precedence < o2.precedence)
                            ) {
                        // then pop o2 off the stack,
                        stack.pop_back();
                        // onto the output queue;
                        queue.push_back(o2);

                        continue;
                    }

                    // @@ otherwise, exit.
                    break;
                }

                // push o1 onto the stack.
                stack.push_back(o1);
            }
                break;

            case Token::Type::LeftParen:
                // If token is left parenthesis, then push it onto the stack
                stack.push_back(token);
                break;

            case Token::Type::RightParen:
                // If token is right parenthesis:
            {
                bool match = false;
                while (!stack.empty()) {
                    // Until the token at the top of the stack
                    // is a left parenthesis,
                    const auto tos = stack.back();
                    if (tos.type != Token::Type::LeftParen) {
                        // pop operators off the stack
                        stack.pop_back();
                        // onto the output queue.
                        queue.push_back(tos);
                    }

                    // Pop the left parenthesis from the stack,
                    // but not onto the output queue.
                    stack.pop_back();
                    match = true;
                    break;
                }

                if (!match && stack.empty()) {
                    // If the stack runs out without finding a left parenthesis,
                    // then there are mismatched parentheses.
                    printf("RightParen error (%s)\n", token.str.c_str());
                    exit(0);
                }
            }
                break;

            default:
                printf("error (%s)\n", token.str.c_str());
                exit(0);
                break;
        }
    }

    // When there are no more tokens to read:
    //   While there are still operator tokens in the stack:
    while (!stack.empty()) {
        // If the operator token on the top of the stack is a parenthesis,
        // then there are mismatched parentheses.
        if (stack.back().type == Token::Type::LeftParen) {
            printf("Mismatched parentheses error\n");
            exit(0);
        }

        // Pop the operator onto the output queue.
        queue.push_back(std::move(stack.back()));
        stack.pop_back();
    }

    //Exit.
    return queue;
}

double ShuntingYard::calculateExpression(const vector<string> &words, int index) {
    string exp = makeValueFromExpression(words, index);
    const auto tokens = exprToTokens(exp);
    auto queuePrefix = shuntingYard(tokens);
    Expression *expression = makeOneExpression(queuePrefix);
    double value = expression->calculate();
    delete (expression);

    return value;
}

Expression *ShuntingYard::makeOneExpression(deque<Token> tokens) {
    vector<Expression *> stack;

    while (!tokens.empty()) {
        const auto token = tokens.front();
        tokens.pop_front();
        switch (token.type) {
            case Token::Type::Number: {
                Expression *exp = new Number(stod(token.str));
                stack.push_back(exp);
                expressionsList.push_back(exp);
                break;
            }

            case Token::Type::Operator: {
                const auto rhs = stack.back();
                stack.pop_back();
                const auto lhs = stack.back();
                if (stack.size() != 0) {
                    stack.pop_back();
                }

                switch (token.str[0]) {

                    case '*': {
                        Expression *expression = new Mul(lhs, rhs);
                        stack.push_back(expression);
                        expressionsList.push_back(expression);
                        break;
                    }
                    case '/': {
                        Expression *expression = new Div(lhs, rhs);
                        stack.push_back(expression);
                        expressionsList.push_back(expression);
                        break;
                    }
                    case '+': {
                        Expression *expression = new Plus(lhs, rhs);
                        stack.push_back(expression);
                        expressionsList.push_back(expression);
                        break;
                    }
                    case '-': {
                        Expression *expression = new Minus(lhs, rhs);
                        stack.push_back(expression);
                        expressionsList.push_back(expression);
                        break;
                    }

                    default:
                        printf("Operator error [%s]\n", token.str.c_str());
                        exit(0);
                        break;
                }
            }
                break;

            default:
                printf("Token error\n");
                exit(0);
        }
    }
    return stack.back();
}

ShuntingYard::ShuntingYard(const unordered_map<string, Var *> &varsMap) : varsMap(varsMap) {}

ShuntingYard::~ShuntingYard() {
//    list<Expression *>::iterator it = expressionsList.begin();
//
//    for (; it != expressionsList.end(); ++it) {
//        delete (&it);
//    }

    expressionsList.clear();
}





