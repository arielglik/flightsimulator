//
// Created by ariel on 12/14/18.
//

#ifndef FLIGHTSIMULATOR_SHUNTINGYARD_H
#define FLIGHTSIMULATOR_SHUNTINGYARD_H

#include <vector>
#include <string>
#include <deque>
#include "Expression.h"
#include "Token.cpp"
#include "Number.cpp"
#include "BinaryExpression.h"
#include "Mul.cpp"
#include "Div.cpp"
#include "Plus.cpp"
#include "Minus.cpp"
#include "Number.cpp"
#include "Var.h"
#include <tr1/unordered_map>
#include <list>

using namespace std;
using namespace tr1;

class ShuntingYard {


private:
    list<Expression *> expressionsList;

    const unordered_map<string, Var *> &varsMap;

    string makeValueFromExpression(const vector<string> &words, int index);

    string fromManyToOneString(const vector<string> &words, int index);

    deque<Token> exprToTokens(const std::string &expr);

    deque<Token> shuntingYard(const std::deque<Token> &tokens);

    Expression *makeOneExpression(deque<Token> tokens);

    bool isOperator(char ch);

    bool isNumber(char ch);

public:
    ShuntingYard(const unordered_map<string, Var *> &varsMap);

    double calculateExpression(const vector<string> &words, int index);

    virtual ~ShuntingYard();
};


#endif //FLIGHTSIMULATOR_SHUNTINGYARD_H
