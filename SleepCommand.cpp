//
// Created by nir on 12/20/18.
//

#include "SleepCommand.h"

SleepCommand::SleepCommand(unordered_map<string, Var *> &varsMap) : varsMap(varsMap) {
    this->shuntingYard = new ShuntingYard(varsMap);
}

int SleepCommand::execute(const vector<string> &words, int index) {
    string str = words.at(index); // assuming index is on the 'sleep'
    int counter = 0;
    unordered_map<string, Var *>::const_iterator it = varsMap.find(words.at(index + 1));

    double value = shuntingYard->calculateExpression(words, index + 1);
    usleep(static_cast<unsigned  int>(value * 1000));

    for (index; str != ";"; index++, counter++) {
        str = words.at(index);
    }

    return counter;
}

SleepCommand::~SleepCommand() {
    delete (this->shuntingYard);

}
