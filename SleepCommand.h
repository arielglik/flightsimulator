//
// Created by nir on 12/20/18.
//

#ifndef FLIGHTSIMULATOR_SLEEPCOMMAND_H
#define FLIGHTSIMULATOR_SLEEPCOMMAND_H


#include "Command.h"
#include "ShuntingYard.h"
#include "OpenServerCommand.h"
#include "ShuntingYard.h"
#include <tr1/unordered_map>
#include <unistd.h>

using namespace std;
using namespace tr1;

class SleepCommand : public Command {
    unordered_map<string, Var *> &varsMap;
    ShuntingYard *shuntingYard;

public:
    SleepCommand(unordered_map<string, Var *> &varsMap);

    int execute(const vector<string> &words, int index) override;

    virtual ~SleepCommand();
};


#endif //FLIGHTSIMULATOR_SLEEPCOMMAND_H
