#include <string>

using namespace std;

class Token {
public:
    enum Type {
        Unknown = 0,
        Number,
        Operator,
        LeftParen,
        RightParen,
    };

    Token(Type t, const std::string &s, int prec = -1, bool ra = false)
            : type{t}, str(s), precedence{prec}, rightAssociative{ra} {}

    Type type{Type::Unknown};
    std::string str{};
    int precedence{-1};
    bool rightAssociative{false};
};