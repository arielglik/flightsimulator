
#include "Var.h"

using namespace std;


Var::Var(const string &name, double value, ClientParams *clientParams) : name(name), value(value),
                                                                         clientParams(clientParams) {
    isBinded = false;
    bindAddress = "";
}


Var::Var(const string &name, const string &bindAddress, ClientParams *clientParams) : name(name),
                                                                                      bindAddress(bindAddress),
                                                                                      clientParams(clientParams) {
    isBinded = true;
}


const double Var::getValue() {
    if (isBinded && isInXML) {
        double x = *this->bindedValue;
        return *this->bindedValue;
    } else if (isBinded && !isInXML) {
        return getValueFromSimulator();
    } else {
        return this->value;
    }
}

void Var::setValue(double value) {
    isBinded = false;
    this->value = value;
}


void Var::setBinding(string bindedAddress, double *value, bool isInXML) {
    this->isBinded = true;
    this->bindedValue = value;
    this->bindAddress = bindedAddress;
    this->isInXML = isInXML;
}

void Var::setBindedValue(double *value) {

    clientParams->text = "";
    clientParams->text = "set " + bindAddress + " " + to_string(*value) + "\r\n";
    clientParams->isNeedToSet = true;
    while (clientParams->isNeedToSet) {}
    this->bindedValue = value;
}

bool Var::getIsBinded() const {
    return isBinded;
}

double Var::getValueFromSimulator() {
    clientParams->text = "";
    clientParams->text = "get " + bindAddress + "\r\n";
    clientParams->isNeedToSet = true;
    int i, j;
    while (clientParams->isNeedToSet) {} // wait for simulator response.

    string text = clientParams->text;
    for (i = 0; i < text.length(); i++) {
        if (text[i] == '\'') {
            i++;
            break;
        }
    }

    for (j = i + 1; j < text.length(); j++) {
        if (text[j] == '\'') {
            break;
        }
    }
    string str = text.substr(i, j - i);

    return stod(str);
}

const string &Var::getName() const {
    return name;
}

const string &Var::getBindAddress() const {
    return bindAddress;
}


