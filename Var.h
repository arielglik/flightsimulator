//
// Created by ariel on 12/9/18.
//

#ifndef FLIGHTSIMULATOR_VARIABLECENTER_H
#define FLIGHTSIMULATOR_VARIABLECENTER_H


#include <stdio.h>
#include <fstream>
#include <string>
#include "Command.h"
#include "ConnectCommand.h"

using namespace std;

class Var {

private:
    string name;
    double value;
    double *bindedValue;
    string bindAddress;
    bool isBinded;
    bool isInXML;
    struct ClientParams *clientParams;


public:
    Var(const string &name, double value, ClientParams *clientParams);

    Var(const string &name, const string &bindAddress, ClientParams *clientParams);

    //void findValue(const string &bindAddress);
    const double getValue();

    void setValue(double value);

    void setBinding(string bindedAddress, double *value, bool isInXML);

    void setBindedValue(double *value);

    bool getIsBinded() const;

    const string &getBindAddress() const;

    const string &getName() const;

    double getValueFromSimulator();
};

#endif //FLIGHTSIMULATOR_VARIABLECENTER_H
