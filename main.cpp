#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include "OpenServerCommand.h"
#include "LoopCommand.h"
#include "DefineVarCommand.h"
#include "ConnectCommand.h"
#include "Var.h"
#include "IfCommand.h"
#include "Lexer.h"
#include "Parser.h"

using namespace std;
using namespace tr1;

int main(int argc, char *argv[]) {
    ifstream input;
    bool isFinished = false;
    bool isReadFromFile = false;
    vector<string> words;
    Lexer *lexer = new Lexer(words);
    Parser *parser = new Parser(words);

    string path = "";

    if (argc > 1) {
        path = argv[1];
        isReadFromFile = true;
    }

    if (isReadFromFile) {
        words = lexer->lexerFromFile(path, isReadFromFile);
        parser->run();

    } else {
        while (!isFinished) {
            words = lexer->lexerFromFile("", isReadFromFile);
            isFinished = parser->run();
        }
    }
    delete (lexer);
    delete (parser);


    return 0;
}

